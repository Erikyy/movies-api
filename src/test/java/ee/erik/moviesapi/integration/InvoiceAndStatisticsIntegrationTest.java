package ee.erik.moviesapi.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieStatDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceItemDto;
import ee.erik.moviesapi.service.InvoiceService;
import ee.erik.moviesapi.service.MovieService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class InvoiceAndStatisticsIntegrationTest {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private MovieService movieService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldCreateReadDeleteInvoice() throws Exception {
        //database is initialized with some data using DatabaseInitializer
        MovieDto firstMovie = assertDoesNotThrow(() -> this.movieService.findById(0L));
        MovieDto secondMovie = assertDoesNotThrow(() -> this.movieService.findById(1L));


        Date startDate = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        Date endDate = Date.from(LocalDateTime.now().plusWeeks(5).toInstant(ZoneOffset.UTC));
        CreateInvoiceDto createInvoiceDto = new CreateInvoiceDto(
                List.of(
                        new CreateInvoiceItemDto(startDate, endDate, firstMovie.getId()),
                        new CreateInvoiceItemDto(startDate, endDate, secondMovie.getId())
                )
        );

        MvcResult addResult = this.mockMvc.perform(
                        post("/api/v1/invoices")
                                .content(new ObjectMapper().writeValueAsString(createInvoiceDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        InvoiceDto addedResult = new ObjectMapper().readValue(addResult.getResponse().getContentAsString(), InvoiceDto.class);
        assertThat(addedResult).isNotNull();

        MvcResult getByIdResult = this.mockMvc.perform(
                        get("/api/v1/admin/invoices/{id}", addedResult.getId())
                                .content(new ObjectMapper().writeValueAsString(createInvoiceDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        InvoiceDto getByIdFoundResult = new ObjectMapper().readValue(getByIdResult.getResponse().getContentAsString(), InvoiceDto.class);
        assertThat(getByIdFoundResult).isNotNull();
        assertThat(getByIdFoundResult).isEqualTo(addedResult);

        this.mockMvc.perform(
                        delete("/api/v1/admin/invoices/{id}", getByIdFoundResult.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        assertThat(this.invoiceService.findAllInvoices()).doesNotContain(getByIdFoundResult);
    }

    @Test
    public void shouldGetMostAndLeastPopularMovie() throws Exception {
    //database is initialized with some data using DatabaseInitializer
        MovieDto firstMovie = assertDoesNotThrow(() -> this.movieService.findById(0L));
        MovieDto secondMovie = assertDoesNotThrow(() -> this.movieService.findById(1L));


        Date startDate = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC));
        Date endDate = Date.from(LocalDateTime.now().plusWeeks(5).toInstant(ZoneOffset.UTC));
        CreateInvoiceDto createInvoiceDto = new CreateInvoiceDto(
                List.of(
                        new CreateInvoiceItemDto(startDate, endDate, firstMovie.getId()),
                        new CreateInvoiceItemDto(startDate, endDate, secondMovie.getId())
                )
        );
        CreateInvoiceDto createSecondInvoiceDto = new CreateInvoiceDto(
                List.of(
                        new CreateInvoiceItemDto(startDate, endDate, firstMovie.getId())
                )
        );

        //add two invoices
        this.invoiceService.addInvoice(createInvoiceDto);
        this.invoiceService.addInvoice(createSecondInvoiceDto);



        MvcResult getMostPopularResult = this.mockMvc.perform(
                        get("/api/v1/statistics/most-popular")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieStatDto foundMostPopularResult = new ObjectMapper().readValue(getMostPopularResult.getResponse().getContentAsString(), MovieStatDto.class);
        assertThat(foundMostPopularResult).isNotNull();
        //added two invoices with 2 occurrences of the first movie
        assertThat(foundMostPopularResult.getCount()).isEqualTo(2);
        assertThat(foundMostPopularResult.getMovie()).isEqualTo(firstMovie);

        MvcResult getLeastPopularResult = this.mockMvc.perform(
                        get("/api/v1/statistics/least-popular")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieStatDto foundLeastPopularResult = new ObjectMapper().readValue(getLeastPopularResult.getResponse().getContentAsString(), MovieStatDto.class);
        assertThat(foundLeastPopularResult).isNotNull();
        //added two invoices with 2 occurrences of the first movie
        assertThat(foundLeastPopularResult.getCount()).isEqualTo(1);
        assertThat(foundLeastPopularResult.getMovie()).isEqualTo(secondMovie);
    }

    @Test
    public void statisticsShouldThrow404ErrorWhenNoInvoicesArePresent() throws Exception {
        //delete all invoices
        for (InvoiceDto invoiceDto : this.invoiceService.findAllInvoices()) {
            this.invoiceService.delete(invoiceDto.getId());
        }

        this.mockMvc.perform(
                        get("/api/v1/statistics/most-popular")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andReturn();

        this.mockMvc.perform(
                        get("/api/v1/statistics/least-popular")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound())
                .andReturn();
    }
}
