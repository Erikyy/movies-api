package ee.erik.moviesapi.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.erik.documentdb.config.EnableDocumentDb;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieMetadataDto;
import ee.erik.moviesapi.dto.create.CreateMovieRatingDto;
import ee.erik.moviesapi.dto.update.UpdateMovieDto;
import ee.erik.moviesapi.dto.update.UpdateMovieGenreDto;
import ee.erik.moviesapi.dto.update.UpdateMovieMetadataDto;
import ee.erik.moviesapi.service.MovieGenreService;
import ee.erik.moviesapi.service.MovieService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MovieIntegrationTest {

    @Autowired
    private MovieService movieService;

    @Autowired
    private MovieGenreService movieGenreService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldCreateReadUpdateDeleteGenres() throws Exception {

        CreateMovieGenreDto createMovieGenreDto = new CreateMovieGenreDto("Test");

        MvcResult addResult = this.mockMvc.perform(
                post("/api/v1/admin/genres")
                        .content(new ObjectMapper().writeValueAsString(createMovieGenreDto))
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieGenreDto addedResult = new ObjectMapper().readValue(addResult.getResponse().getContentAsString(), MovieGenreDto.class);

        assertThat(addedResult).isNotNull();

        UpdateMovieGenreDto updateMovieGenreDto = new UpdateMovieGenreDto(addedResult.getId(), "Test 2");

        MvcResult updateResult = this.mockMvc.perform(
                        put("/api/v1/admin/genres/{id}", updateMovieGenreDto.getId())
                                .content(new ObjectMapper().writeValueAsString(updateMovieGenreDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieGenreDto updatedResult = new ObjectMapper().readValue(updateResult.getResponse().getContentAsString(), MovieGenreDto.class);

        assertThat(updatedResult).isNotNull();
        assertThat(updatedResult).isNotEqualTo(addedResult);

        MvcResult getByIdResult = this.mockMvc.perform(
                        get("/api/v1/genres/{id}", updatedResult.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieGenreDto getByIdFoundResult = new ObjectMapper().readValue(getByIdResult.getResponse().getContentAsString(), MovieGenreDto.class);

        assertThat(getByIdFoundResult).isNotNull();
        assertThat(getByIdFoundResult).isEqualTo(updatedResult);

        MvcResult getAllResult = this.mockMvc.perform(
                        get("/api/v1/genres")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        List<MovieGenreDto> getAllFoundResult = List.of(new ObjectMapper().readValue(getAllResult.getResponse().getContentAsString(), MovieGenreDto[].class));

        assertThat(getAllFoundResult).isNotEmpty();
        assertThat(getAllFoundResult).contains(getByIdFoundResult);

        this.mockMvc.perform(
                        delete("/api/v1/admin/genres/{id}", getByIdFoundResult.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        assertThat(this.movieGenreService.findAll()).doesNotContain(getByIdFoundResult);
    }

    @Test
    public void shouldCreateReadUpdateDeleteMovies() throws Exception {

        MovieGenreDto movieGenreDto = assertDoesNotThrow(() -> this.movieGenreService.findById(0L));

        CreateMovieMetadataDto createMovieMetadataDto = new CreateMovieMetadataDto();
        createMovieMetadataDto.setDescription("Some description");
        createMovieMetadataDto.setReleaseDate(Date.from(LocalDateTime.now().minusWeeks(30).toInstant(ZoneOffset.UTC)));
        //genre name not important can be set to null or whatever
        //database is initialized with data so data does exist there
        createMovieMetadataDto.setGenre(List.of(movieGenreDto));
        createMovieMetadataDto.setRatings(List.of(new CreateMovieRatingDto("Some Value", "Some value")));

        CreateMovieDto createMovieDto = new CreateMovieDto("Test", createMovieMetadataDto);


        MvcResult addResult = this.mockMvc.perform(
                        post("/api/v1/admin/movies")
                                .content(new ObjectMapper().writeValueAsString(createMovieDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieDto addedResult = new ObjectMapper().readValue(addResult.getResponse().getContentAsString(), MovieDto.class);
        assertThat(addedResult).isNotNull();

        UpdateMovieMetadataDto updateMovieMetadataDto = new UpdateMovieMetadataDto();
        updateMovieMetadataDto.setId(addedResult.getMetadata().getId());
        updateMovieMetadataDto.setDescription(createMovieMetadataDto.getDescription());
        updateMovieMetadataDto.setReleaseDate(createMovieMetadataDto.getReleaseDate());
        updateMovieMetadataDto.setGenre(createMovieMetadataDto.getGenre());
        updateMovieMetadataDto.setRatings(List.of());
        UpdateMovieDto updateMovieDto = new UpdateMovieDto(addedResult.getId(), "Test new updated", updateMovieMetadataDto);

        MvcResult updateResult = this.mockMvc.perform(
                        put("/api/v1/admin/movies/{id}", addedResult.getId())
                                .content(new ObjectMapper().writeValueAsString(updateMovieDto))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieDto updatedResult = new ObjectMapper().readValue(updateResult.getResponse().getContentAsString(), MovieDto.class);

        assertThat(updatedResult).isNotNull();
        assertThat(updatedResult).isNotEqualTo(addedResult);

        MvcResult getByIdResult = this.mockMvc.perform(
                        get("/api/v1/movies/{id}", updatedResult.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        MovieDto getByIdFoundResult = new ObjectMapper().readValue(getByIdResult.getResponse().getContentAsString(), MovieDto.class);
        assertThat(getByIdFoundResult).isNotNull();
        assertThat(getByIdFoundResult).isEqualTo(updatedResult);

        MvcResult getAllResult = this.mockMvc.perform(
                        get("/api/v1/movies")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        List<MovieDto> getAllFoundResult = List.of(new ObjectMapper().readValue(getAllResult.getResponse().getContentAsString(), MovieDto[].class));

        assertThat(getAllFoundResult).isNotEmpty();
        assertThat(getAllFoundResult).contains(getByIdFoundResult);

        MvcResult getAllByGenreResult = this.mockMvc.perform(
                        get("/api/v1/movies?genres={genre}", movieGenreDto.getName())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andReturn();

        List<MovieDto> getAllByGenreFoundResult = List.of(new ObjectMapper().readValue(getAllByGenreResult.getResponse().getContentAsString(), MovieDto[].class));

        assertThat(getAllByGenreFoundResult).isNotEmpty();
        assertThat(getAllByGenreFoundResult).contains(getByIdFoundResult);

        this.mockMvc.perform(
                        delete("/api/v1/admin/movies/{id}", getByIdFoundResult.getId())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk());

        assertThat(this.movieService.findAll()).doesNotContain(getByIdFoundResult);
    }

}
