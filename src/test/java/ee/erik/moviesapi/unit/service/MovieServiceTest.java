package ee.erik.moviesapi.unit.service;

import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.create.CreateMovieMetadataDto;
import ee.erik.moviesapi.dto.create.CreateMovieRatingDto;
import ee.erik.moviesapi.dto.update.UpdateMovieDto;
import ee.erik.moviesapi.dto.update.UpdateMovieMetadataDto;
import ee.erik.moviesapi.dto.update.UpdateMovieRatingDto;
import ee.erik.moviesapi.entity.Movie;
import ee.erik.moviesapi.entity.MovieGenre;
import ee.erik.moviesapi.entity.MovieMetadata;
import ee.erik.moviesapi.entity.MovieRating;
import ee.erik.moviesapi.repository.MovieGenreRepository;
import ee.erik.moviesapi.repository.MovieMetadataRepository;
import ee.erik.moviesapi.repository.MovieRatingRepository;
import ee.erik.moviesapi.repository.MovieRepository;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.service.impl.MovieServiceImpl;
import ee.erik.moviesapi.utils.PriceCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovieServiceTest {

    @Mock
    private MovieRepository movieRepository;

    @Mock
    private MovieGenreRepository movieGenreRepository;

    @Mock
    private MovieMetadataRepository movieMetadataRepository;

    @Mock
    private MovieRatingRepository movieRatingRepository;

    @InjectMocks
    private MovieService movieService = new MovieServiceImpl();

    private Movie testMovie;

    private MovieMetadata testMetadata;

    private MovieGenre testMovieGenre;

    private MovieRating testMovieRating;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        this.testMovieGenre = new MovieGenre(0L, "Test");
        this.testMovieRating = new MovieRating(0L, "Some value", "Some value");

        this.testMetadata = new MovieMetadata();
        testMetadata.setId(0L);
        testMetadata.setReleaseDate(new SimpleDateFormat("dd/MM/yyyy").format(Date.from(LocalDateTime.now().minusWeeks(30).toInstant(ZoneOffset.UTC))));
        testMetadata.setDescription("Some description");
        testMetadata.setGenre(List.of(0L));
        testMetadata.setRatings(List.of(0L));

        this.testMovie = new Movie(0L, "Test", 0L);
    }

    @Test
    public void shouldCreateNewMovie() {
        given(this.movieGenreRepository.findAll()).willReturn(List.of(testMovieGenre));
        given(this.movieRatingRepository.insert(any())).willReturn(testMovieRating);

        given(this.movieMetadataRepository.insert(any())).willReturn(this.testMetadata);
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        this.testMovie.setId(null);

        given(this.movieRepository.insert(this.testMovie)).willReturn(this.testMovie);
        CreateMovieMetadataDto createMovieMetadataDto = new CreateMovieMetadataDto();
        createMovieMetadataDto.setDescription("Some description");
        createMovieMetadataDto.setReleaseDate(Date.from(LocalDateTime.now().minusWeeks(30).toInstant(ZoneOffset.UTC)));
        createMovieMetadataDto.setGenre(List.of(new MovieGenreDto(0L, "Test")));
        createMovieMetadataDto.setRatings(List.of(new CreateMovieRatingDto("Some Value", "Some value")));


        MovieDto movieDto = this.movieService.addMovie(
                new CreateMovieDto("Test", createMovieMetadataDto)
        );

        assertThat(movieDto.getTitle()).isEqualTo(testMovie.getTitle());
        assertThat(movieDto.getMetadata().getPrice()).isEqualTo(5D);

    }

    @Test
    public void shouldUpdateMovie() {

        this.testMovie.setTitle("Test update");
        given(this.movieGenreRepository.findAll()).willReturn(List.of(this.testMovieGenre));
        given(this.movieRepository.findById(0L)).willReturn(Optional.of(this.testMovie));
        this.testMovieRating.setId(0L);
        given(this.movieRatingRepository.update(this.testMovieRating)).willReturn(this.testMovieRating);
        given(this.movieMetadataRepository.update(this.testMetadata)).willReturn(this.testMetadata);
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));
        given(this.movieRatingRepository.findById(0L)).willReturn(Optional.of(this.testMovieRating));
        given(this.movieRepository.update(this.testMovie)).willReturn(this.testMovie);

        UpdateMovieMetadataDto updateMovieMetadataDto = new UpdateMovieMetadataDto();
        updateMovieMetadataDto.setId(0L);
        updateMovieMetadataDto.setDescription("Some description");
        updateMovieMetadataDto.setReleaseDate(Date.from(LocalDateTime.now().minusWeeks(30).toInstant(ZoneOffset.UTC)));
        updateMovieMetadataDto.setGenre(List.of(new MovieGenreDto(0L, "Test")));
        updateMovieMetadataDto.setRatings(List.of(new UpdateMovieRatingDto(0L, "Some value", "Some value")));

        UpdateMovieDto updateMovieDto = new UpdateMovieDto();
        updateMovieDto.setId(0L);
        updateMovieDto.setTitle("Test update");

        updateMovieDto.setMetadata(updateMovieMetadataDto);

        MovieDto movieDto = this.movieService.updateMovie(0L, updateMovieDto);

        assertThat(movieDto).isNotNull();
        assertThat(movieDto.getTitle()).isEqualTo("Test update");

        assertThat(movieDto.getMetadata().getPrice()).isEqualTo(5D);
    }

    @Test
    public void shouldListAllMovies() {
        given(this.movieRepository.findAll()).willReturn(List.of(this.testMovie));
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));
        given(this.movieRatingRepository.findById(0L)).willReturn(Optional.of(this.testMovieRating));

        List<MovieDto> movieDtoList = this.movieService.findAll();

        assertThat(movieDtoList).isNotEmpty();
    }

    @Test
    public void shouldListMoviesByGenre() {
        given(this.movieGenreRepository.findAll()).willReturn(List.of(this.testMovieGenre));
        given(this.movieRepository.findAll()).willReturn(List.of(this.testMovie));
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));
        given(this.movieRatingRepository.findById(0L)).willReturn(Optional.of(this.testMovieRating));

        List<MovieDto> movieDtoList = this.movieService.findAllByGenre("Test");

        assertThat(movieDtoList).isNotEmpty();
    }

    @Test
    public void shouldFindMovieById() {
        given(this.movieRepository.findById(0L)).willReturn(Optional.of(this.testMovie));
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));
        given(this.movieRatingRepository.findById(0L)).willReturn(Optional.of(this.testMovieRating));

        assertDoesNotThrow(() -> {
            //cannot be null otherwise it will throw NotFoundException
            MovieDto movieDto = this.movieService.findById(0L);
            assertThat(movieDto.getId()).isEqualTo(0L);
        });
    }

    @Test
    public void shouldDeleteMovie() {
        given(this.movieRepository.findById(0L)).willReturn(Optional.of(this.testMovie));
        given(this.movieMetadataRepository.findById(0L)).willReturn(Optional.of(this.testMetadata));
        given(this.movieRatingRepository.findById(0L)).willReturn(Optional.of(this.testMovieRating));

        assertDoesNotThrow(() -> {
            this.movieService.deleteMovie(0L);
        });

        verify(this.movieRepository, times(1)).delete(this.testMovie);
    }
}
