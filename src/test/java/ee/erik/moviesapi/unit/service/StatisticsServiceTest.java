package ee.erik.moviesapi.unit.service;

import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.InvoiceItemDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieStatDto;
import ee.erik.moviesapi.exception.NotFoundException;
import ee.erik.moviesapi.service.InvoiceService;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.service.StatisticsService;
import ee.erik.moviesapi.service.impl.StatisticsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class StatisticsServiceTest {

    @Mock
    private InvoiceService invoiceService;

    @Mock
    private MovieService movieService;

    @InjectMocks
    private StatisticsService statisticsService = new StatisticsServiceImpl();

    private MovieDto testMovie1;

    private MovieDto testMovie2;

    private InvoiceDto testInvoice1;

    private InvoiceDto testInvoice2;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        this.testMovie1 = new MovieDto(0L, "Test 1", null);
        this.testMovie2 = new MovieDto(1L, "Test 2", null);

        this.testInvoice1 = new InvoiceDto(0L, 10D, List.of(
                new InvoiceItemDto(new Date(), new Date(), 5D, this.testMovie1),
                new InvoiceItemDto(new Date(), new Date(), 5D, this.testMovie2)
        ));

        this.testInvoice2 = new InvoiceDto(1L, 5D, List.of(
                new InvoiceItemDto(new Date(), new Date(), 5D, this.testMovie1)
        ));
    }

    @Test
    public void serviceShouldReturnMostPopularMovie() {
        given(this.invoiceService.findAllInvoices()).willReturn(List.of(this.testInvoice1, this.testInvoice2));
        given(this.movieService.findById(this.testMovie1.getId())).willReturn(this.testMovie1);

        MovieStatDto popular = this.statisticsService.getMostPopularMovie();

        assertThat(popular.getMovie()).isEqualTo(this.testMovie1);
        assertThat(popular.getCount()).isEqualTo(2);

    }

    @Test
    public void serviceShouldReturnLeastPopularMovie() {
        given(this.invoiceService.findAllInvoices()).willReturn(List.of(this.testInvoice1, this.testInvoice2));
        given(this.movieService.findById(this.testMovie2.getId())).willReturn(this.testMovie2);

        MovieStatDto popular = this.statisticsService.getLeastPopularMovie();

        assertThat(popular.getMovie()).isEqualTo(this.testMovie2);
        assertThat(popular.getCount()).isEqualTo(1);
    }

    @Test
    public void serviceShouldThrowExceptionWhenEmptyListIsProvided() {
        given(this.invoiceService.findAllInvoices()).willReturn(List.of());

        assertThrows(NotFoundException.class, () -> {
            this.statisticsService.getLeastPopularMovie();
        });
        assertThrows(NotFoundException.class, () -> {
            this.statisticsService.getMostPopularMovie();
        });
    }
}
