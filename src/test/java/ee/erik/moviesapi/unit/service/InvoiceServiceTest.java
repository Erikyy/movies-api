package ee.erik.moviesapi.unit.service;


import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.InvoiceItemDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieMetadataDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceItemDto;
import ee.erik.moviesapi.entity.Invoice;
import ee.erik.moviesapi.entity.InvoiceItem;
import ee.erik.moviesapi.repository.InvoiceItemRepository;
import ee.erik.moviesapi.repository.InvoiceRepository;
import ee.erik.moviesapi.service.InvoiceService;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.service.impl.InvoiceServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTest {

    @Mock
    private InvoiceRepository invoiceRepository;

    @Mock
    private InvoiceItemRepository invoiceItemRepository;

    @Mock
    private MovieService movieService;

    @InjectMocks
    private InvoiceService invoiceService = new InvoiceServiceImpl();

    private MovieDto testMovieDto;

    private InvoiceDto testInvoiceDto;

    private Invoice invoice;

    private InvoiceItem invoiceItem;

    private Date startDate;

    private Date endDate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);

        this.startDate = Date.from(LocalDateTime.of(2022, Month.FEBRUARY, 6, 0, 0).toInstant(ZoneOffset.UTC));
        this.endDate = Date.from(LocalDateTime.of(2022, Month.JUNE, 12, 0, 0).toInstant(ZoneOffset.UTC));


        MovieMetadataDto movieMetadataDto = new MovieMetadataDto();
        movieMetadataDto.setReleaseDate(Date.from(LocalDateTime.of(2021, 2, 21, 0, 0).toInstant(ZoneOffset.UTC)));
        this.testMovieDto = new MovieDto(0L, "Test", movieMetadataDto);

        this.testInvoiceDto = new InvoiceDto(0L, 20.47D, List.of(
                new InvoiceItemDto(
                        this.startDate,
                        this.endDate,
                        20.47D,
                        this.testMovieDto
                )
        ));

        this.invoiceItem = new InvoiceItem();
        this.invoiceItem.setId(0L);
        this.invoiceItem.setPrice(20.47D);
        this.invoiceItem.setRentalStartDate(new SimpleDateFormat("dd/MM/yyyy").format(this.startDate));
        this.invoiceItem.setRentalEndDate(new SimpleDateFormat("dd/MM/yyyy").format(this.endDate));
        this.invoiceItem.setRentedMovie(this.testMovieDto.getId());

        this.invoice = new Invoice();
        this.invoice.setId(0L);
        this.invoice.setTotalPrice(20.47D);
        this.invoice.setInvoiceItems(List.of(this.invoiceItem.getId()));
    }

    @Test
    public void serviceShouldAddInvoice() {
        given(this.movieService.findById(0L)).willReturn(this.testMovieDto);
        given(this.invoiceItemRepository.insert(any())).willReturn(this.invoiceItem);
        given(this.invoiceRepository.insert(any())).willReturn(this.invoice);

        CreateInvoiceDto createInvoiceDto = new CreateInvoiceDto(
                List.of(
                        new CreateInvoiceItemDto(
                                this.startDate,
                                this.endDate,
                                0L
                        )
                )
        );

        InvoiceDto invoiceDto = assertDoesNotThrow(() -> this.invoiceService.addInvoice(createInvoiceDto));

        assertThat(invoiceDto).isEqualTo(this.testInvoiceDto);
    }

    @Test
    public void serviceShouldFindAllInvoices() {
        given(this.invoiceRepository.findAll()).willReturn(List.of(this.invoice));
        given(this.invoiceItemRepository.findById(0L)).willReturn(Optional.of(this.invoiceItem));
        given(this.movieService.findById(this.invoiceItem.getRentedMovie())).willReturn(this.testMovieDto);

        List<InvoiceDto> invoices = assertDoesNotThrow(() -> this.invoiceService.findAllInvoices());

        assertThat(invoices).isNotEmpty();
        assertThat(invoices).contains(this.testInvoiceDto);
    }

    @Test
    public void serviceShouldFindInvoiceById() {

        given(this.invoiceRepository.findById(0L)).willReturn(Optional.of(this.invoice));
        given(this.invoiceItemRepository.findById(0L)).willReturn(Optional.of(this.invoiceItem));
        given(this.movieService.findById(this.invoiceItem.getRentedMovie())).willReturn(this.testMovieDto);

        InvoiceDto invoice = assertDoesNotThrow(() -> this.invoiceService.findById(0L));

        assertThat(invoice).isEqualTo(this.testInvoiceDto);
    }

    @Test
    public void serviceShouldDeleteInvoice() {
        given(this.invoiceRepository.findById(0L)).willReturn(Optional.of(this.invoice));
        given(this.invoiceItemRepository.findById(0L)).willReturn(Optional.of(this.invoiceItem));

        assertDoesNotThrow(() -> {
            this.invoiceService.delete(0L);
        });
    }


}
