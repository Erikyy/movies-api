package ee.erik.moviesapi.unit.service;


import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.update.UpdateMovieGenreDto;
import ee.erik.moviesapi.entity.MovieGenre;
import ee.erik.moviesapi.repository.MovieGenreRepository;
import ee.erik.moviesapi.service.MovieGenreService;
import ee.erik.moviesapi.service.impl.MovieGenreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class MovieGenreServiceTest {

    @Mock
    private MovieGenreRepository movieGenreRepository;

    @InjectMocks
    private MovieGenreService movieGenreService = new MovieGenreServiceImpl();

    private MovieGenreDto testMovieGenreDto;

    private MovieGenre testMovieGenre;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        this.testMovieGenreDto = new MovieGenreDto(0L, "Test");
        this.testMovieGenre = new MovieGenre(0L, "Test");
    }

    @Test
    public void serviceShouldReturnAllGenres() {
        given(this.movieGenreRepository.findAll()).willReturn(List.of(this.testMovieGenre));

        List<MovieGenreDto> foundAll = this.movieGenreService.findAll();

        assertThat(foundAll).contains(this.testMovieGenreDto);
    }

    @Test
    public void serviceShouldReturnGenreById() {
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));

        MovieGenreDto found = assertDoesNotThrow(() -> this.movieGenreService.findById(0L));

        assertThat(found).isEqualTo(this.testMovieGenreDto);

    }

    @Test
    public void serviceShouldAddNewGenre() {
        given(this.movieGenreRepository.insert(any())).willReturn(this.testMovieGenre);
        CreateMovieGenreDto createMovieGenreDto = new CreateMovieGenreDto("Test");

        MovieGenreDto saved = assertDoesNotThrow(() -> this.movieGenreService.addGenre(createMovieGenreDto));

        assertThat(saved).isEqualTo(this.testMovieGenreDto);
    }

    @Test
    public void serviceShouldUpdateGenre() {
        given(this.movieGenreRepository.update(this.testMovieGenre)).willReturn(this.testMovieGenre);
        UpdateMovieGenreDto updateMovieGenreDto = new UpdateMovieGenreDto(0L, "Test");
        MovieGenreDto updated = assertDoesNotThrow(() -> this.movieGenreService.updateGenre(0L, updateMovieGenreDto));

        assertThat(updated).isEqualTo(this.testMovieGenreDto);
    }

    @Test
    public void serviceShouldDeleteGenre() {
        given(this.movieGenreRepository.findById(0L)).willReturn(Optional.of(this.testMovieGenre));

        assertDoesNotThrow(() -> {
            this.movieGenreService.deleteGenre(0L);
        });
    }
}
