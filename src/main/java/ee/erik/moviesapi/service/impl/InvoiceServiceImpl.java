package ee.erik.moviesapi.service.impl;

import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.InvoiceItemDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceItemDto;
import ee.erik.moviesapi.entity.Invoice;
import ee.erik.moviesapi.entity.InvoiceItem;
import ee.erik.moviesapi.exception.InternalServerException;
import ee.erik.moviesapi.exception.NotFoundException;
import ee.erik.moviesapi.repository.InvoiceItemRepository;
import ee.erik.moviesapi.repository.InvoiceRepository;
import ee.erik.moviesapi.service.InvoiceService;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.utils.PriceCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InvoiceServiceImpl implements InvoiceService {


    private InvoiceRepository invoiceRepository;
    private InvoiceItemRepository invoiceItemRepository;

    @Autowired
    public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Autowired
    public void setInvoiceItemRepository(InvoiceItemRepository invoiceItemRepository) {
        this.invoiceItemRepository = invoiceItemRepository;
    }

    //use movie service to fetch movie by id
    private MovieService movieService;

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public InvoiceDto addInvoice(CreateInvoiceDto createInvoiceDto) {
        Invoice invoice = new Invoice();
        List<Long> invoiceItemIds = new ArrayList<>();
        List<InvoiceItemDto> invoiceItems = new ArrayList<>();
        Double finalPrice = 0D;

        for (CreateInvoiceItemDto createInvoiceItemDto : createInvoiceDto.getCreateInvoiceItems()) {
            InvoiceItem invoiceItem = new InvoiceItem();

            MovieDto movieDto = this.movieService.findById(createInvoiceItemDto.getMovieId());

            invoiceItem.setRentedMovie(movieDto.getId());
            invoiceItem.setRentalStartDate(new SimpleDateFormat("dd/MM/yyyy").format(createInvoiceItemDto.getStartDate()));
            invoiceItem.setRentalEndDate(new SimpleDateFormat("dd/MM/yyyy").format(createInvoiceItemDto.getEndDate()));
            invoiceItem.setPrice(PriceCalculator.calculateMoviePriceForInvoice(movieDto.getMetadata().getReleaseDate(), createInvoiceItemDto.getStartDate(), createInvoiceItemDto.getEndDate()));

            InvoiceItem saved = this.invoiceItemRepository.insert(invoiceItem);

            if (saved == null) {
                throw new InternalServerException("Failed to save new invoice item");
            }

            invoiceItemIds.add(saved.getId());
            InvoiceItemDto invoiceItemDto = new InvoiceItemDto();
            invoiceItemDto.setRentedMovie(movieDto);
            invoiceItemDto.setPrice(saved.getPrice());
            try {
                DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                invoiceItemDto.setRentalStartDate(Date.from(LocalDate.parse(saved.getRentalStartDate(), format).atStartOfDay().toInstant(ZoneOffset.UTC)));
                invoiceItemDto.setRentalEndDate(Date.from(LocalDate.parse(saved.getRentalEndDate(), format).atStartOfDay().toInstant(ZoneOffset.UTC)));
            } catch (Exception e) {
                throw new InternalServerException("Something went wrong with date parsing");
            }
            invoiceItems.add(invoiceItemDto);
            finalPrice = finalPrice + saved.getPrice();
        }

        invoice.setInvoiceItems(invoiceItemIds);
        invoice.setTotalPrice(finalPrice);

        Invoice saved = this.invoiceRepository.insert(invoice);

        if (saved == null) {
            throw new InternalServerException("Failed to save new invoice");
        }

        InvoiceDto invoiceDto = new InvoiceDto();
        invoiceDto.setInvoiceItems(invoiceItems);
        invoiceDto.setTotalPrice(finalPrice);
        invoiceDto.setId(saved.getId());
        return invoiceDto;
    }

    @Override
    public List<InvoiceDto> findAllInvoices() {
        List<Invoice> invoices = this.invoiceRepository.findAll();
        List<InvoiceDto> invoiceDtoList = new ArrayList<>();
        for (Invoice invoice : invoices) {
            List<InvoiceItemDto> invoiceItems = this.getInvoiceItemsFromInvoice(invoice);
            InvoiceDto invoiceDto = new InvoiceDto();
            invoiceDto.setId(invoice.getId());
            invoiceDto.setTotalPrice(invoice.getTotalPrice());
            invoiceDto.setInvoiceItems(invoiceItems);
            invoiceDtoList.add(invoiceDto);
        }

        return invoiceDtoList;
    }

    @Override
    public InvoiceDto findById(Long id) {
        Optional<Invoice> invoice = this.invoiceRepository.findById(id);
        if (invoice.isPresent()) {
            List<InvoiceItemDto> invoiceItems = this.getInvoiceItemsFromInvoice(invoice.get());
            InvoiceDto invoiceDto = new InvoiceDto();
            invoiceDto.setId(invoice.get().getId());
            invoiceDto.setTotalPrice(invoice.get().getTotalPrice());
            invoiceDto.setInvoiceItems(invoiceItems);
            return invoiceDto;
        } else {
            throw new NotFoundException("Invoice: " + id + " not found");
        }
    }

    @Override
    public void delete(Long id) {
        Optional<Invoice> invoice = this.invoiceRepository.findById(id);
        if (invoice.isPresent()) {
            for (Long invoiceItemId : invoice.get().getInvoiceItems()) {
                Optional<InvoiceItem> invoiceItem = this.invoiceItemRepository.findById(invoiceItemId);
                if (invoiceItem.isPresent()) {
                    this.invoiceItemRepository.delete(invoiceItem.get());
                } else {
                    throw new NotFoundException("Invoice Item: " + id + " not found");
                }
            }
            this.invoiceRepository.delete(invoice.get());
        } else {
            throw new NotFoundException("Invoice: " + id + " not found");
        }
    }

    private List<InvoiceItemDto> getInvoiceItemsFromInvoice(Invoice invoice) {
        return invoice.getInvoiceItems().stream().map(p -> {
            Optional<InvoiceItem> invoiceItem = this.invoiceItemRepository.findById(p);
            if (invoiceItem.isEmpty()) {
                //throw error
                throw new NotFoundException("The invoice item: " + p + " does not exist");
            } else {
                MovieDto movieDto = this.movieService.findById(invoiceItem.get().getRentedMovie());
                InvoiceItemDto invoiceItemDto = new InvoiceItemDto();
                invoiceItemDto.setRentedMovie(movieDto);
                invoiceItemDto.setPrice(invoiceItem.get().getPrice());
                try {
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    invoiceItemDto.setRentalStartDate(Date.from(LocalDate.parse(invoiceItem.get().getRentalStartDate(), format).atStartOfDay().toInstant(ZoneOffset.UTC)));
                    invoiceItemDto.setRentalEndDate(Date.from(LocalDate.parse(invoiceItem.get().getRentalEndDate(), format).atStartOfDay().toInstant(ZoneOffset.UTC)));
                } catch (Exception e) {
                    throw new InternalServerException("Something went wrong with date parsing");
                }
                return invoiceItemDto;
            }

        }).collect(Collectors.toList());
    }
}
