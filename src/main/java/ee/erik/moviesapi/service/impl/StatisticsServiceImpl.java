package ee.erik.moviesapi.service.impl;

import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.InvoiceItemDto;
import ee.erik.moviesapi.dto.MovieStatDto;
import ee.erik.moviesapi.exception.NotFoundException;
import ee.erik.moviesapi.service.InvoiceService;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    private InvoiceService invoiceService;

    private MovieService movieService;

    @Autowired
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public MovieStatDto getMostPopularMovie() {
        List<Long> allMovieIdOccurrences = this.allMovieIdOccurrences();

        Optional<Map.Entry<Long, Long>> mostOccurringMovieId = allMovieIdOccurrences.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());

        if (mostOccurringMovieId.isEmpty()) {
            throw new NotFoundException("No most popular movie exist");
        }

        return new MovieStatDto(this.movieService.findById(mostOccurringMovieId.get().getKey()), Math.toIntExact(mostOccurringMovieId.get().getValue()));
    }

    @Override
    public MovieStatDto getLeastPopularMovie() {
        List<Long> allMovieIdOccurrences = this.allMovieIdOccurrences();

        Optional<Map.Entry<Long, Long>> leastOccurringMovieId = allMovieIdOccurrences.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .min(Map.Entry.comparingByValue());

        if (leastOccurringMovieId.isEmpty()) {
            throw new NotFoundException("No most popular movie exist");
        }

        return new MovieStatDto(this.movieService.findById(leastOccurringMovieId.get().getKey()), Math.toIntExact(leastOccurringMovieId.get().getValue()));
    }

    private List<Long> allMovieIdOccurrences() {
        List<InvoiceDto> invoices = this.invoiceService.findAllInvoices();

        if (invoices.isEmpty()) {
            throw new NotFoundException("No most popular movie exist");
        }

        List<Long> allMovieIdOccurrences = new ArrayList<>();

        for (InvoiceDto invoice : invoices) {
            for (InvoiceItemDto invoiceItem : invoice.getInvoiceItems()) {
                allMovieIdOccurrences.add(invoiceItem.getRentedMovie().getId());
            }
        }
        return allMovieIdOccurrences;
    }
}
