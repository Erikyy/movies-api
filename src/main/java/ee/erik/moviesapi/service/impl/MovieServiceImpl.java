package ee.erik.moviesapi.service.impl;

import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.MovieMetadataDto;
import ee.erik.moviesapi.dto.MovieRatingDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.update.UpdateMovieDto;
import ee.erik.moviesapi.entity.Movie;
import ee.erik.moviesapi.entity.MovieGenre;
import ee.erik.moviesapi.entity.MovieMetadata;
import ee.erik.moviesapi.entity.MovieRating;
import ee.erik.moviesapi.exception.InternalServerException;
import ee.erik.moviesapi.exception.NotFoundException;
import ee.erik.moviesapi.repository.MovieGenreRepository;
import ee.erik.moviesapi.repository.MovieMetadataRepository;
import ee.erik.moviesapi.repository.MovieRatingRepository;
import ee.erik.moviesapi.repository.MovieRepository;
import ee.erik.moviesapi.service.MovieService;
import ee.erik.moviesapi.utils.PriceCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {


    private MovieRepository movieRepository;
    private MovieGenreRepository movieGenreRepository;
    private MovieMetadataRepository movieMetadataRepository;
    private MovieRatingRepository movieRatingRepository;

    @Autowired
    public void setMovieRepository(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Autowired
    public void setMovieGenreRepository(MovieGenreRepository movieGenreRepository) {
        this.movieGenreRepository = movieGenreRepository;
    }

    @Autowired
    public void setMovieMetadataRepository(MovieMetadataRepository movieMetadataRepository) {
        this.movieMetadataRepository = movieMetadataRepository;
    }

    @Autowired
    public void setMovieRatingRepository(MovieRatingRepository movieRatingRepository) {
        this.movieRatingRepository = movieRatingRepository;
    }

    @Override
    public List<MovieDto> findAll() {
        List<Movie> movies = this.movieRepository.findAll();
        return movies.stream().map(this::convertMovieToDto).collect(Collectors.toSet()).stream().toList();
    }

    @Override
    public MovieDto findById(Long id) {
        Optional<Movie> movie = this.movieRepository.findById(id);
        if (movie.isPresent()) {
            return movie.map(this::convertMovieToDto).get();
        } else {
            throw new NotFoundException("Movie with id: " + id + " not found");
        }
    }

    @Override
    public List<MovieDto> findAllByGenre(String genre) {
        Optional<MovieGenreDto> movieGenre = this.movieGenreRepository.findAll().stream().filter(p ->
            p.getName().equals(genre)
        ).findFirst().map(p -> new MovieGenreDto(p.getId(), p.getName()));
        return this.findAll().stream().filter(p ->
            movieGenre.map(movieGenreDto -> p.getMetadata().getGenre().contains(movieGenreDto)).orElse(true)
        ).collect(Collectors.toList());
    }

    @Override
    public MovieDto addMovie(CreateMovieDto movie) {
        Movie movieEntity = this.convertCreateMovieDtoToEntity(movie);
        return this.convertMovieToDto(this.movieRepository.insert(movieEntity));
    }

    @Override
    public MovieDto updateMovie(Long id, UpdateMovieDto updateMovieDto) {
        if (this.movieRepository.findById(id).isEmpty()) {
            throw new NotFoundException("Movie: " + id + " not found");
        } else {
            Movie updateMovie = this.convertUpdateMovieDtoToEntity(updateMovieDto);
            Movie updatedMovie = this.movieRepository.update(updateMovie);
            if (updatedMovie == null) {
                throw new InternalServerException("Failed to update movie");
            }
            return this.convertMovieToDto(updatedMovie);
        }
    }

    @Override
    public void deleteMovie(Long id) {
        Optional<Movie> movie = this.movieRepository.findById(id);
        if (movie.isPresent()) {
            Optional<MovieMetadata> metadata = this.movieMetadataRepository.findById(movie.get().getMetadata());
            if (metadata.isPresent()) {
                for (Long ratingId : metadata.get().getRatings()) {
                    Optional<MovieRating> rating = this.movieRatingRepository.findById(ratingId);
                    if (rating.isPresent()) {
                        this.movieRatingRepository.delete(rating.get());
                    } else {
                        throw new NotFoundException("Rating: " + ratingId + " not found in movie metadata");
                    }
                }
            } else {
                throw new NotFoundException("Movie metadata: " + movie.get().getMetadata() + " not found");
            }
            this.movieRepository.delete(movie.get());
        } else {
            throw new NotFoundException("Movie: " + id + " not found");
        }
    }

    private MovieDto convertMovieToDto(Movie movie) {
        MovieDto movieDto = new MovieDto();
        Optional<MovieMetadata> metadata = this.movieMetadataRepository.findById(movie.getMetadata());
        if (metadata.isPresent()) {
            MovieMetadata movieMetadata = metadata.get();
            MovieMetadataDto movieMetadataDto = new MovieMetadataDto();
            movieMetadataDto.setId(movieMetadata.getId());
            try {
                DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                movieMetadataDto.setReleaseDate(Date.from(LocalDate.parse(movieMetadata.getReleaseDate(), format).atStartOfDay().toInstant(ZoneOffset.UTC)));
            } catch (Exception e) {
                throw new InternalServerException("Date conversion error");
            }
            movieMetadataDto.setDescription(movieMetadata.getDescription());
            movieMetadataDto.setRuntime(movieMetadata.getRuntime());
            movieMetadataDto.setDirector(movieMetadata.getDirector());
            movieMetadataDto.setCountry(movieMetadata.getCountry());
            movieMetadataDto.setRated(movieMetadata.getRated());
            movieMetadataDto.setPoster(movieMetadata.getPoster());
            movieMetadataDto.setAwards(movieMetadata.getAwards());
            movieMetadataDto.setMetascore(movieMetadata.getMetascore());
            movieMetadataDto.setImdbRating(movieMetadata.getImdbRating());
            movieMetadataDto.setImdbVotes(movieMetadata.getImdbVotes());

            movieMetadataDto.setPrice(PriceCalculator.calculateMoviePrice(movieMetadataDto.getReleaseDate()));

            movieMetadataDto.setActors(movieMetadata.getActors());
            movieMetadataDto.setWriters(movieMetadata.getWriters());
            movieMetadataDto.setLanguages(movieMetadata.getLanguages());

            movieMetadataDto.setGenre(movieMetadata.getGenre().stream().map(p -> {
                Optional<MovieGenre> genre = this.movieGenreRepository.findById(p);
                return genre.map( movieGenre -> new MovieGenreDto(movieGenre.getId(), movieGenre.getName())).orElse(null);
            }).collect(Collectors.toSet()).stream().toList());

            movieMetadataDto.setRatings(movieMetadata.getRatings().stream().map(p -> {
                Optional<MovieRating> rating = this.movieRatingRepository.findById(p);
                return rating.map(movieRating -> new MovieRatingDto(movieRating.getId(), movieRating.getSource(), movieRating.getValue())).orElse(null);
            }).collect(Collectors.toList()));

            movieDto.setId(movie.getId());
            movieDto.setTitle(movie.getTitle());
            movieDto.setMetadata(movieMetadataDto);
        } else {
            throw new NotFoundException("Movie metadata not found");
        }
        return movieDto;
    }

    private Movie convertCreateMovieDtoToEntity(CreateMovieDto createMovieDto) {
        List<Long> movieGenres = this.movieGenreRepository.findAll().stream().map(MovieGenre::getId).filter(id ->
                createMovieDto.getMetadata().getGenre().stream().anyMatch(p2 -> Objects.equals(p2.getId(), id))
        ).toList();

        List<Long> ratings = createMovieDto.getMetadata().getRatings().stream().map(p -> {
            MovieRating rating = new MovieRating();
            rating.setValue(p.getValue());
            rating.setSource(p.getSource());
            return this.movieRatingRepository.insert(rating).getId();
        }).collect(Collectors.toList());
        MovieMetadata metadata = new MovieMetadata();
        metadata.setReleaseDate(new SimpleDateFormat("dd/MM/yyyy").format(createMovieDto.getMetadata().getReleaseDate()));
        metadata.setRuntime(createMovieDto.getMetadata().getRuntime());
        metadata.setDescription(createMovieDto.getMetadata().getDescription());
        metadata.setDirector(createMovieDto.getMetadata().getDirector());
        metadata.setCountry(createMovieDto.getMetadata().getCountry());
        metadata.setRated(createMovieDto.getMetadata().getRated());
        metadata.setPoster(createMovieDto.getMetadata().getPoster());
        metadata.setAwards(createMovieDto.getMetadata().getAwards());
        metadata.setMetascore(createMovieDto.getMetadata().getMetascore());
        metadata.setImdbRating(createMovieDto.getMetadata().getImdbRating());
        metadata.setImdbVotes(createMovieDto.getMetadata().getImdbVotes());
        metadata.setActors(createMovieDto.getMetadata().getActors());
        metadata.setWriters(createMovieDto.getMetadata().getWriters());
        metadata.setLanguages(createMovieDto.getMetadata().getLanguages());
        metadata.setRatings(ratings);
        metadata.setGenre(movieGenres);
        MovieMetadata movieMetadata = this.movieMetadataRepository.insert(metadata);
        if (movieMetadata == null) {
            throw new InternalServerException("Failed to create movie metadata");
        }
        Movie movie = new Movie();
        movie.setTitle(createMovieDto.getTitle());
        movie.setMetadata(movieMetadata.getId());
        return movie;
    }

    /**
     *
     * @param updateMovieDto
     * @return
     */
    private Movie convertUpdateMovieDtoToEntity(UpdateMovieDto updateMovieDto) {
        List<Long> movieGenres = this.movieGenreRepository.findAll().stream().map(MovieGenre::getId).filter(id ->
                updateMovieDto.getMetadata().getGenre().stream().anyMatch(p2 -> Objects.equals(p2.getId(), id))
        ).toList();
        List<Long> ratings = updateMovieDto.getMetadata().getRatings().stream().map(p -> {
            MovieRating rating = new MovieRating();
            rating.setValue(p.getValue());
            rating.setSource(p.getSource());
            if (p.getId() == null) {
                MovieRating movieRating = this.movieRatingRepository.insert(rating);
                if (movieRating == null) {
                    throw new InternalServerException("Failed to create movie rating");
                }
                return movieRating.getId();
            } else {
                rating.setId(p.getId());
                MovieRating movieRating = this.movieRatingRepository.update(rating);
                if (movieRating == null) {
                    throw new InternalServerException("Failed to update movie rating");
                }

                return movieRating.getId();
            }

        }).collect(Collectors.toList());
        MovieMetadata metadata = new MovieMetadata();
        metadata.setId(updateMovieDto.getMetadata().getId());
        metadata.setReleaseDate(new SimpleDateFormat("dd/MM/yyyy").format(updateMovieDto.getMetadata().getReleaseDate()));
        metadata.setRuntime(updateMovieDto.getMetadata().getRuntime());
        metadata.setDescription(updateMovieDto.getMetadata().getDescription());
        metadata.setDirector(updateMovieDto.getMetadata().getDirector());
        metadata.setCountry(updateMovieDto.getMetadata().getCountry());
        metadata.setRated(updateMovieDto.getMetadata().getRated());
        metadata.setPoster(updateMovieDto.getMetadata().getPoster());
        metadata.setAwards(updateMovieDto.getMetadata().getAwards());
        metadata.setMetascore(updateMovieDto.getMetadata().getMetascore());
        metadata.setImdbRating(updateMovieDto.getMetadata().getImdbRating());
        metadata.setImdbVotes(updateMovieDto.getMetadata().getImdbVotes());
        metadata.setActors(updateMovieDto.getMetadata().getActors());
        metadata.setWriters(updateMovieDto.getMetadata().getWriters());
        metadata.setLanguages(updateMovieDto.getMetadata().getLanguages());
        metadata.setRatings(ratings);
        metadata.setGenre(movieGenres);
        MovieMetadata movieMetadata = this.movieMetadataRepository.update(metadata);
        if (movieMetadata == null) {
            throw new InternalServerException("Failed to update movie metadata");
        }
        Movie movie = new Movie();
        movie.setId(updateMovieDto.getId());
        movie.setTitle(updateMovieDto.getTitle());
        movie.setMetadata(movieMetadata.getId());
        return movie;
    }
}
