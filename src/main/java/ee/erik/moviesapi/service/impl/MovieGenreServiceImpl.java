package ee.erik.moviesapi.service.impl;

import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.update.UpdateMovieGenreDto;
import ee.erik.moviesapi.entity.MovieGenre;
import ee.erik.moviesapi.exception.InternalServerException;
import ee.erik.moviesapi.exception.NotFoundException;
import ee.erik.moviesapi.repository.MovieGenreRepository;
import ee.erik.moviesapi.service.MovieGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieGenreServiceImpl implements MovieGenreService {

    private MovieGenreRepository movieGenreRepository;

    @Autowired
    public void setMovieGenreRepository(MovieGenreRepository movieGenreRepository) {
        this.movieGenreRepository = movieGenreRepository;
    }

    @Override
    public List<MovieGenreDto> findAll() {
        return this.movieGenreRepository.findAll().stream().map(p -> new MovieGenreDto(p.getId(), p.getName())).toList();
    }

    @Override
    public MovieGenreDto findById(Long id) {
        Optional<MovieGenre> movieGenre = this.movieGenreRepository.findById(id);
        if (movieGenre.isPresent()) {
            return movieGenre.map(p -> new MovieGenreDto(p.getId(), p.getName())).get();
        } else {
            throw new NotFoundException("Movie genre with id: " + id + " not found");
        }
    }

    @Override
    public MovieGenreDto addGenre(CreateMovieGenreDto movieGenreDto) {
        MovieGenre movieGenre = new MovieGenre();
        movieGenre.setName(movieGenreDto.getName());
        MovieGenre saved = this.movieGenreRepository.insert(movieGenre);
        if (saved == null) {
            throw new InternalServerException("Failed to create movie genre");
        }
        return new MovieGenreDto(saved.getId(), saved.getName());
    }

    @Override
    public MovieGenreDto updateGenre(Long id, UpdateMovieGenreDto updateMovieGenreDto) {
        MovieGenre updatedGenre = this.movieGenreRepository.update(new MovieGenre(id, updateMovieGenreDto.getName()));
        if (updatedGenre == null) {
            throw new InternalServerException("Failed to create movie genre");
        }
        return new MovieGenreDto(updatedGenre.getId(), updatedGenre.getName());
    }

    @Override
    public void deleteGenre(Long id) {
        Optional<MovieGenre> movieGenre = this.movieGenreRepository.findById(id);
        if (movieGenre.isPresent()) {
            this.movieGenreRepository.delete(movieGenre.get());
        } else {
            throw new NotFoundException("Movie genre with id: " + id + " not found");
        }
    }
}
