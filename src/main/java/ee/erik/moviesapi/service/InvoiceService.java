package ee.erik.moviesapi.service;

import ee.erik.moviesapi.dto.InvoiceDto;
import ee.erik.moviesapi.dto.create.CreateInvoiceDto;

import java.util.List;

public interface InvoiceService {

    InvoiceDto addInvoice(CreateInvoiceDto createInvoiceDto);

    List<InvoiceDto> findAllInvoices();

    InvoiceDto findById(Long id);

    void delete(Long id);
}
