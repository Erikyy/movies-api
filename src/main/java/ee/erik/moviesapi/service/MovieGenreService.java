package ee.erik.moviesapi.service;

import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.update.UpdateMovieGenreDto;

import java.util.List;

public interface MovieGenreService {

    List<MovieGenreDto> findAll();

    MovieGenreDto findById(Long id);

    MovieGenreDto addGenre(CreateMovieGenreDto movieGenreDto);

    MovieGenreDto updateGenre(Long id, UpdateMovieGenreDto updateMovieGenreDto);

    void deleteGenre(Long id);
}
