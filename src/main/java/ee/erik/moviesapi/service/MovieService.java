package ee.erik.moviesapi.service;


import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.update.UpdateMovieDto;

import java.util.List;

public interface MovieService {

    List<MovieDto> findAll();

    MovieDto findById(Long id);

    /**
     * Gets all movies by genre
     * @param genre
     * @return
     */
    List<MovieDto> findAllByGenre(String genre);

    MovieDto addMovie(CreateMovieDto movie);

    /**
     * Updates existing movie.
     * Note! if there is a desire to add more ratings
     * just set the rating id to null, otherwise set this to
     * existing rating id
     * @param id movie id
     * @param updateMovieDto update data
     * @return updated movie
     */
    MovieDto updateMovie(Long id, UpdateMovieDto updateMovieDto);
    void deleteMovie(Long id);
}
