package ee.erik.moviesapi.service;

import ee.erik.moviesapi.dto.MovieStatDto;

public interface StatisticsService {

    /**
     * Returns most popular/rented movie
     */
    MovieStatDto getMostPopularMovie();

    /**
     * Returns least popular/rented movie
     */
    MovieStatDto getLeastPopularMovie();
}
