package ee.erik.moviesapi.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class PriceCalculator {

    /**
     * This here is lazily implemented price classes
     */
    private static final Double NEW_MOVIE_PRICE = 5.00;
    private static final Double REGULAR_MOVIE_PRICE = 3.49;
    private static final Double OLD_MOVIE_PRICE = 1.99;

    public static Double calculateMoviePrice(Date movieDate) {
        long weeksPassed = getNumberOfWeeksBetweenDates(movieDate, Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC)));
        if (weeksPassed < 52) {
            return NEW_MOVIE_PRICE;
        } else if (weeksPassed > 52 && weeksPassed <= 156) {
            return REGULAR_MOVIE_PRICE;
        } else if (weeksPassed >= 156) {
            return OLD_MOVIE_PRICE;
        } else {
            return 0D;
        }
    }

    /**
     * Calculates movie price for invoice
     *
     * @param movieDate
     * @param startDate Start date is usually today
     * @param endDate
     * @return
     */
    public static Double calculateMoviePriceForInvoice(Date movieDate, Date startDate, Date endDate) {
        double finalPrice = 0D;
        long numberOfWeeksToRent = getNumberOfWeeksBetweenDates(startDate, endDate);

        LocalDateTime startWeek = LocalDateTime.ofInstant(startDate.toInstant(), ZoneId.systemDefault());
        for (int i = 0; i <= numberOfWeeksToRent; i++) {
            //here increment price accordingly
            LocalDateTime thisWeek = startWeek.plusWeeks(i);
            long weeksPassed = getNumberOfWeeksBetweenDates(movieDate, Date.from(thisWeek.toInstant(ZoneOffset.UTC)));

            if (weeksPassed < 52) {
                finalPrice = finalPrice + NEW_MOVIE_PRICE;
            } else if (weeksPassed > 52 && weeksPassed <= 156) {
                finalPrice = finalPrice + REGULAR_MOVIE_PRICE;
            } else if (weeksPassed > 156) {
                finalPrice = finalPrice + OLD_MOVIE_PRICE;
            }
        }
        return finalPrice;
    }

    private static long getNumberOfWeeksBetweenDates(Date first, Date second) {
        LocalDateTime start = LocalDateTime.ofInstant(first.toInstant(), ZoneId.systemDefault());
        LocalDateTime end = LocalDateTime.ofInstant(second.toInstant(), ZoneId.systemDefault());

        return ChronoUnit.WEEKS.between(start, end);
    }
}
