package ee.erik.moviesapi.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieMetadataDto {
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date releaseDate;
    private String description;
    private String runtime;
    private String director;
    private String country;
    private String rated;
    private String poster;
    private String awards;
    private String metascore;
    private String imdbRating;
    private String imdbVotes;
    private Double price;
    private List<MovieGenreDto> genre;
    private List<String> actors;
    private List<String> writers;
    private List<String> languages;
    private List<MovieRatingDto> ratings;
}
