package ee.erik.moviesapi.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceItemDto {
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date rentalStartDate;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date rentalEndDate;
    private Double price;
    private MovieDto rentedMovie;
}
