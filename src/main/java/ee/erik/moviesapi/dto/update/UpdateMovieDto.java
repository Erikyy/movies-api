package ee.erik.moviesapi.dto.update;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateMovieDto {
    private Long id;
    private String title;
    private UpdateMovieMetadataDto metadata;
}
