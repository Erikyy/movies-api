package ee.erik.moviesapi.dto.update;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateMovieRatingDto {
    private Long id;
    private String source;
    private String value;
}
