package ee.erik.moviesapi.dto.create;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateInvoiceDto {
    private List<CreateInvoiceItemDto> createInvoiceItems;
}
