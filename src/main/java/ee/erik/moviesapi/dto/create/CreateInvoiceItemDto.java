package ee.erik.moviesapi.dto.create;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateInvoiceItemDto {
    private Date startDate;
    private Date endDate;
    private Long movieId;
}
