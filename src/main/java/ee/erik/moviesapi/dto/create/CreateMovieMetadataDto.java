package ee.erik.moviesapi.dto.create;

import ee.erik.moviesapi.dto.MovieGenreDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateMovieMetadataDto {
    private Date releaseDate;
    private String description;
    private String runtime;
    private String director;
    private String country;
    private String rated;
    private String poster;
    private String awards;
    private String metascore;
    private String imdbRating;
    private String imdbVotes;
    private List<MovieGenreDto> genre;
    private List<String> actors;
    private List<String> writers;
    private List<String> languages;
    private List<CreateMovieRatingDto> ratings;
}
