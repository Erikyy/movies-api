package ee.erik.moviesapi.dto.create;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateMovieDto {
    private String title;
    private CreateMovieMetadataDto metadata;
}
