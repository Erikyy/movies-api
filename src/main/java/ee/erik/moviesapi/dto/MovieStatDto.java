package ee.erik.moviesapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieStatDto {

    private MovieDto movie;
    /**
     * this count represent on how many times a movie was rented
     */
    private int count;
}
