package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.Movie;

public interface MovieRepository extends GenericRepository<Movie, Long> {
}
