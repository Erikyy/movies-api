package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.MovieGenre;

public interface MovieGenreRepository extends GenericRepository<MovieGenre, Long> {
}
