package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.MovieRating;

public interface MovieRatingRepository extends GenericRepository<MovieRating, Long> {
}
