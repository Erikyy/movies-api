package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.Invoice;

public interface InvoiceRepository extends GenericRepository<Invoice, Long> {
}
