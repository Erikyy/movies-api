package ee.erik.moviesapi.repository;

import java.util.List;
import java.util.Optional;

/**
 * Here is just base functions for repositories
 * @param <T> Entity type
 * @param <ID> id type
 */
public interface GenericRepository<T, ID> {
    List<T> findAll();
    Optional<T> findById(ID id);
    T insert(T entity);
    T update(T entity);
    void delete(T entity);
}
