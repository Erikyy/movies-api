package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.InvoiceItem;

public interface InvoiceItemRepository extends GenericRepository<InvoiceItem, Long> {

}
