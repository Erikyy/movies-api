package ee.erik.moviesapi.repository;

import ee.erik.moviesapi.entity.MovieMetadata;

public interface MovieMetadataRepository extends GenericRepository<MovieMetadata, Long> {
}
