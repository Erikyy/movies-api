package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.Invoice;
import ee.erik.moviesapi.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class InvoiceRepositoryImpl implements InvoiceRepository {

    private ListGenericDao<Invoice, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<Invoice, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(Invoice.class);
    }

    @Override
    public List<Invoice> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<Invoice> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public Invoice insert(Invoice entity) {
        return this.dao.insert(entity);
    }

    @Override
    public Invoice update(Invoice entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(Invoice entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
