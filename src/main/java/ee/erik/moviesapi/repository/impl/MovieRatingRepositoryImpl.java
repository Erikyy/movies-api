package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.MovieRating;
import ee.erik.moviesapi.repository.MovieRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieRatingRepositoryImpl implements MovieRatingRepository {

    private ListGenericDao<MovieRating, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<MovieRating, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(MovieRating.class);
    }

    @Override
    public List<MovieRating> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<MovieRating> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public MovieRating insert(MovieRating entity) {
        return this.dao.insert(entity);
    }

    @Override
    public MovieRating update(MovieRating entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(MovieRating entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
