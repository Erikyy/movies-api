package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.MovieGenre;
import ee.erik.moviesapi.repository.MovieGenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieGenreRepositoryImpl implements MovieGenreRepository {

    private ListGenericDao<MovieGenre, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<MovieGenre, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(MovieGenre.class);
    }

    @Override
    public List<MovieGenre> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<MovieGenre> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public MovieGenre insert(MovieGenre entity) {
        return this.dao.insert(entity);
    }

    @Override
    public MovieGenre update(MovieGenre entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(MovieGenre entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
