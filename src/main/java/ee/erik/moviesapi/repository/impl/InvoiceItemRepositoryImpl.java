package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.InvoiceItem;
import ee.erik.moviesapi.repository.InvoiceItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class InvoiceItemRepositoryImpl implements InvoiceItemRepository {

    private ListGenericDao<InvoiceItem, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<InvoiceItem, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(InvoiceItem.class);
    }

    @Override
    public List<InvoiceItem> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<InvoiceItem> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public InvoiceItem insert(InvoiceItem entity) {
        return this.dao.insert(entity);
    }

    @Override
    public InvoiceItem update(InvoiceItem entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(InvoiceItem entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
