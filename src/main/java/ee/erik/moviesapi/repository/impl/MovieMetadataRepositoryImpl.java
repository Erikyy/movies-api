package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.MovieMetadata;
import ee.erik.moviesapi.repository.MovieMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieMetadataRepositoryImpl implements MovieMetadataRepository {
    private ListGenericDao<MovieMetadata, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<MovieMetadata, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(MovieMetadata.class);
    }
    @Override
    public List<MovieMetadata> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<MovieMetadata> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public MovieMetadata insert(MovieMetadata entity) {
        return this.dao.insert(entity);
    }

    @Override
    public MovieMetadata update(MovieMetadata entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(MovieMetadata entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
