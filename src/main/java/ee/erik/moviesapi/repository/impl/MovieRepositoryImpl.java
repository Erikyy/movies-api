package ee.erik.moviesapi.repository.impl;

import ee.erik.documentdb.dao.ListGenericDao;
import ee.erik.moviesapi.entity.Movie;
import ee.erik.moviesapi.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

    private ListGenericDao<Movie, Long> dao;

    @Autowired
    public void setDao(ListGenericDao<Movie, Long> dao) {
        this.dao = dao;
        this.dao.setObjectInfo(Movie.class);
    }

    @Override
    public List<Movie> findAll() {
        return this.dao.findAll();
    }

    @Override
    public Optional<Movie> findById(Long id) {
        return this.dao.findById(id);
    }

    @Override
    public Movie insert(Movie entity) {
        return this.dao.insert(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return this.dao.update(entity);
    }

    @Override
    public void delete(Movie entity) {
        this.dao.delete(entity);
    }

    @PreDestroy
    public void destroy() {
        this.dao.destroy();
    }
}
