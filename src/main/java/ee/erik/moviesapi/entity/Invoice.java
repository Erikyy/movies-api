package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DatabaseObject
public class Invoice {

    @Id
    @GeneratedId
    @ObjectProperty(name = "invoice_id")
    private Long id;

    @ObjectProperty(name = "total_price")
    private Double totalPrice;

    @ObjectProperty(name = "invoice_items")
    private List<Long> invoiceItems;

}
