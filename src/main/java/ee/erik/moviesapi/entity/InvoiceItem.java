package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DatabaseObject
public class InvoiceItem {

    @Id
    @GeneratedId
    @ObjectProperty(name = "invoice_id")
    private Long id;

    private String rentalStartDate;

    private String rentalEndDate;

    private Double price;

    @ObjectProperty(name = "invoice_rented_movie")
    private Long rentedMovie;
}
