package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DatabaseObject
public class MovieMetadata {
    @Id
    @GeneratedId
    @ObjectProperty(name = "movie_metadata_id")
    private Long id;

    @ObjectProperty(name = "release_date")
    private String releaseDate;
    private String description;
    private String runtime;
    private String director;
    private String country;
    private String rated;
    private String poster;
    private String awards;
    private String metascore;
    private String imdbRating;
    private String imdbVotes;

    private List<Long> genre;

    private List<String> actors;

    private List<String> writers;

    private List<String> languages;

    private List<Long> ratings;
}
