package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@DatabaseObject
@AllArgsConstructor
@NoArgsConstructor
public class Movie {

    @Id
    @GeneratedId
    @ObjectProperty(name = "movie_id")
    private Long id;

    private String title;

    @ObjectProperty(name = "movie_metadata")
    private Long metadata;
}
