package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.DatabaseObject;
import ee.erik.documentdb.core.annotations.GeneratedId;
import ee.erik.documentdb.core.annotations.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DatabaseObject
public class MovieRating {

    @Id
    @GeneratedId
    private Long id;

    private String source;

    private String value;
}
