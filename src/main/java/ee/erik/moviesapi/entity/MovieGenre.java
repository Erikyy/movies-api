package ee.erik.moviesapi.entity;

import ee.erik.documentdb.core.annotations.DatabaseObject;
import ee.erik.documentdb.core.annotations.GeneratedId;
import ee.erik.documentdb.core.annotations.Id;
import ee.erik.documentdb.core.annotations.ObjectProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DatabaseObject
public class MovieGenre {
    @Id
    @GeneratedId
    @ObjectProperty(name = "movie_id")
    private Long id;

    @ObjectProperty(name = "category_name")
    private String name;
}
