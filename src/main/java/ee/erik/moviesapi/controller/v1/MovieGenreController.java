package ee.erik.moviesapi.controller.v1;

import ee.erik.moviesapi.dto.ErrorResponseDto;
import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.service.MovieGenreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/genres")
@Tag(name = "MovieGenre", description = "MovieGenre routes")
public class MovieGenreController {

    private MovieGenreService movieGenreService;

    @Autowired
    public void setMovieGenreService(MovieGenreService movieGenreService) {
        this.movieGenreService = movieGenreService;
    }

    @Operation(summary = "Find all genres")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(array = @ArraySchema(schema = @Schema(implementation = MovieGenreDto.class)))),
    })
    @GetMapping
    public List<MovieGenreDto> getAllGenres() {
        return this.movieGenreService.findAll();
    }

    @Operation(summary = "Find genre by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieGenreDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @GetMapping("/{id}")
    public MovieGenreDto getGenreById(@PathVariable Long id) {
        return this.movieGenreService.findById(id);
    }
}
