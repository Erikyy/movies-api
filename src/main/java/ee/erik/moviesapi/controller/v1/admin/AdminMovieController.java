package ee.erik.moviesapi.controller.v1.admin;


import ee.erik.moviesapi.dto.ErrorResponseDto;
import ee.erik.moviesapi.dto.MovieDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.update.UpdateMovieDto;
import ee.erik.moviesapi.service.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This controller is for managing movies
 */
@RestController
@RequestMapping("/api/v1/admin/movies")
@Tag(name = "Admin Movie", description = "Admin Movie routes")
public class AdminMovieController {

    private MovieService movieService;

    @Autowired
    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }


    @Operation(summary = "Get all movies", tags = "Admin Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(array = @ArraySchema(schema = @Schema(implementation = MovieDto.class)))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @GetMapping()
    public List<MovieDto> getAllMovies() {
        return this.movieService.findAll();
    }

    @Operation(summary = "Get movie by id", tags = "Admin Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @GetMapping("/{id}")
    public MovieDto getMovieById(@PathVariable Long id) {
        return this.movieService.findById(id);
    }

    @Operation(summary = "Updates movie", tags = "Admin Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @PutMapping("/{id}")
    public MovieDto updateMovie(@PathVariable Long id, @RequestBody UpdateMovieDto movie) {
        return this.movieService.updateMovie(id, movie);
    }

    @Operation(summary = "Creates new movie", tags = "Admin Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @PostMapping()
    public MovieDto createMovie(@RequestBody CreateMovieDto createMovieDto) {
        return this.movieService.addMovie(createMovieDto);
    }

    @Operation(summary = "Deletes a movie", tags = "Admin Movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success"),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable Long id) {
        this.movieService.deleteMovie(id);
    }

}
