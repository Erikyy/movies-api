package ee.erik.moviesapi.controller.v1.admin;

import ee.erik.moviesapi.dto.ErrorResponseDto;
import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.update.UpdateMovieGenreDto;
import ee.erik.moviesapi.service.MovieGenreService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This controller is for managing movie genres
 */
@RestController
@RequestMapping("/api/v1/admin/genres")
@Tag(name = "Admin Genre", description = "Admin Genre routes")
public class AdminGenreController {

    private MovieGenreService movieGenreService;

    @Autowired
    public void setMovieGenreService(MovieGenreService movieGenreService) {
        this.movieGenreService = movieGenreService;
    }

    @Operation(summary = "Get all genres", tags = "Admin Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(array = @ArraySchema(schema = @Schema(implementation = MovieGenreDto.class)))),
    })
    @GetMapping
    public List<MovieGenreDto> getAllGenres() {
        return this.movieGenreService.findAll();
    }

    @Operation(summary = "Get genre by id", tags = "Admin Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieGenreDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @GetMapping("/{id}")
    public MovieGenreDto getGenreById(@PathVariable Long id) {
        return this.movieGenreService.findById(id);
    }

    @Operation(summary = "Add new genre", tags = "Admin Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieGenreDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @PostMapping()
    public MovieGenreDto addGenre(@RequestBody CreateMovieGenreDto createMovieGenreDto) {
        return this.movieGenreService.addGenre(createMovieGenreDto);
    }

    @Operation(summary = "Update genre", tags = "Admin Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = MovieGenreDto.class))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @PutMapping("/{id}")
    public MovieGenreDto updateGenre(@PathVariable Long id, @RequestBody UpdateMovieGenreDto updateMovieGenreDto) {
        return this.movieGenreService.updateGenre(id, updateMovieGenreDto);
    }

    @Operation(summary = "Delete genre", tags = "Admin Genre")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success"),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class))),
            @ApiResponse(responseCode = "500", description = "Internal service error", content = @Content(schema = @Schema(implementation = ErrorResponseDto.class)))
    })
    @DeleteMapping("/{id}")
    public void deleteGenre(@PathVariable Long id) {
        this.movieGenreService.deleteGenre(id);
    }
}
