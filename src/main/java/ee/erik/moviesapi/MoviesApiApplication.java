package ee.erik.moviesapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ee.erik.documentdb.config.EnableDocumentDb;

@SpringBootApplication
@EnableDocumentDb
public class MoviesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoviesApiApplication.class, args);
    }

}
