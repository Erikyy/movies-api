package ee.erik.moviesapi.config;

import ee.erik.moviesapi.dto.MovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieDto;
import ee.erik.moviesapi.dto.create.CreateMovieGenreDto;
import ee.erik.moviesapi.dto.create.CreateMovieMetadataDto;
import ee.erik.moviesapi.dto.create.CreateMovieRatingDto;
import ee.erik.moviesapi.service.MovieGenreService;
import ee.erik.moviesapi.service.MovieService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;


@Configuration
public class DatabaseInitiation {

    @Bean
    CommandLineRunner initMoviesAndGenres(MovieGenreService movieGenreService, MovieService movieService) {
        return args -> {
            if (movieGenreService.findAll().isEmpty() && movieService.findAll().isEmpty()) {
                CreateMovieGenreDto drama = new CreateMovieGenreDto("Drama");
                CreateMovieGenreDto action = new CreateMovieGenreDto("Action");
                CreateMovieGenreDto scifi = new CreateMovieGenreDto("Sci-Fi");
                CreateMovieGenreDto adventure = new CreateMovieGenreDto("Adventure");

                MovieGenreDto savedDrama = movieGenreService.addGenre(drama);
                MovieGenreDto savedAction = movieGenreService.addGenre(action);
                MovieGenreDto savedScifi = movieGenreService.addGenre(scifi);
                MovieGenreDto savedAdventure = movieGenreService.addGenre(adventure);

                //this data is sourced from OMDB Api
                movieService.addMovie(
                        this.createMovie(
                                "Blade Runner",
                                LocalDate.of(1982, 6, 25),
                                "A blade runner must pursue and terminate four replicants who stole a ship in space, and have returned to Earth to find their creator.",
                                "117 min",
                                "Ridley Scott",
                                "United State",
                                "R",
                                "https://m.media-amazon.com/images/M/MV5BNzQzMzJhZTEtOWM4NS00MTdhLTg0YjgtMjM4MDRkZjUwZDBlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg",
                                "Nominated for 2 Oscars. 13 wins & 19 nominations total",
                                "84",
                                "8.1",
                                "765,432",
                                List.of(
                                        savedAction,
                                        savedDrama,
                                        savedScifi
                                ),
                                List.of(
                                        "Harrison Ford",
                                        "Rutger Hauer",
                                        "Sean Young"
                                ),
                                List.of(
                                        "Hampton Fancher",
                                        "David Webb Peoples",
                                        "Philip K. Dick"
                                ),
                                List.of(
                                        "English",
                                        "German",
                                        "Cantonese",
                                        "Japanese",
                                        "Hungarian",
                                        "Arabic",
                                        "Korean"
                                ),
                                List.of(
                                        new CreateMovieRatingDto("Internet Movie Database", "8.1/10"),
                                        new CreateMovieRatingDto("Rotten Tomatoes", "89%"),
                                        new CreateMovieRatingDto("Metacritic", "84/100")
                                )
                        )
                );

                movieService.addMovie(
                        this.createMovie(
                                "Interstellar",
                                LocalDate.of(2014, 11, 7),
                                "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.",
                                "169 min",
                                "Christopher Nolan",
                                "United States, United Kingdom, Canada",
                                "PG-13",
                                "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
                                "Won 1 Oscar. 44 wins & 148 nominations total",
                                "74",
                                "8.6",
                                "1,799,869",
                                List.of(
                                        savedAdventure,
                                        savedDrama,
                                        savedScifi
                                ),
                                List.of(
                                        "Matthew McConaughey",
                                        "Anne Hathaway",
                                        "Jessica Chastain"
                                ),
                                List.of(
                                        "Jonathan Nolan",
                                        "Christopher Nolan"
                                ),
                                List.of(
                                        "English"
                                ),
                                List.of(
                                        new CreateMovieRatingDto("Internet Movie Database", "8.6/10"),
                                        new CreateMovieRatingDto("Rotten Tomatoes", "73%"),
                                        new CreateMovieRatingDto("Metacritic", "74/100")
                                )
                        )
                );
                movieService.addMovie(
                        this.createMovie(
                                "TRON: Legacy",
                                LocalDate.of(2010, 12, 17),
                                "The son of a virtual world designer goes looking for his father and ends up inside the digital world that his father designed. He meets his father's corrupted creation and a unique ally who was born inside the digital world.",
                                "125 min",
                                "Joseph Kosinski",
                                "United States",
                                "PG",
                                "https://m.media-amazon.com/images/M/MV5BMTk4NTk4MTk1OF5BMl5BanBnXkFtZTcwNTE2MDIwNA@@._V1_SX300.jpg",
                                "Nominated for 1 Oscar. 10 wins & 51 nominations total",
                                "49",
                                "6.8",
                                "337,600",
                                List.of(
                                        savedAction,
                                        savedAdventure,
                                        savedScifi
                                ),
                                List.of(
                                        "Jeff Bridges",
                                        "Garrett Hedlund",
                                        "Olivia Wilde"
                                ),
                                List.of(
                                        "Edward Kitsis",
                                        "Adam Horowitz",
                                        "Brian Klugman"
                                ),
                                List.of("English"),
                                List.of(
                                        new CreateMovieRatingDto("Internet Movie Database", "6.8/10"),
                                        new CreateMovieRatingDto("Rotten Tomatoes", "51%"),
                                        new CreateMovieRatingDto("Metacritic", "49/100")
                                )
                        )
                );
                movieService.addMovie(
                        this.createMovie(
                                "The Lord of the Rings: The Fellowship of the Ring",
                                LocalDate.of(2001, 12, 19),
                                "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.",
                                "178 min",
                                "Peter Jackson",
                                "New Zealand, United States",
                                "PG-13",
                                "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SX300.jpg",
                                "Won 4 Oscars. 121 wins & 126 nominations total",
                                "92",
                                "8.8",
                                "1,858,615",
                                List.of(
                                        savedAction,
                                        savedAdventure,
                                        savedDrama
                                ),
                                List.of(
                                        "Elijah Wood",
                                        "Ian McKellen",
                                        "Orlando Bloom"
                                ),
                                List.of(
                                        "J.R.R. Tolkien",
                                        "Fran Walsh",
                                        "Philippa Boyens"
                                ),
                                List.of(
                                        "English",
                                        "Sindarin"
                                ),
                                List.of(
                                        new CreateMovieRatingDto("Internet Movie Database", "8.8/10"),
                                        new CreateMovieRatingDto("Rotten Tomatoes", "91%"),
                                        new CreateMovieRatingDto("Metacritic", "92/100")
                                )
                        )
                );
            }

        };
    }


    private CreateMovieDto createMovie(
            String title,
            LocalDate releaseDate,
            String description,
            String runtime,
            String director,
            String country,
            String rated,
            String poster,
            String awards,
            String metascore,
            String imdbRating,
            String imdbVotes,
            List<MovieGenreDto> genres,
            List<String> actors,
            List<String> writers,
            List<String> languages,
            List<CreateMovieRatingDto> ratings
    ) {

        CreateMovieMetadataDto createMovieMetadataDto = new CreateMovieMetadataDto(
                Date.from(releaseDate.atStartOfDay().toInstant(ZoneOffset.UTC)),
            description,
            runtime,
            director,
            country,
            rated,
            poster,
            awards,
            metascore,
            imdbRating,
            imdbVotes,
            genres,
            actors,
            writers,
            languages,
            ratings
        );

        return new CreateMovieDto(
                title,
                createMovieMetadataDto
        );
    }
}
