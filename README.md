# movies-api

Fujitsu Java exercise.

- [movies-api](#movies-api)
  - [Introduction](#introduction)
  - [Requirements](#requirements)
  - [Setting up](#setting-up)
  - [Running](#running)
  - [Testing](#testing)

## Introduction

Database implementation is here https://gitlab.com/Erikyy/document-db

Simple movies api.

## Requirements

Java 17

## Setting up

First clone this repository using `--recursive` flag.

The database file flag is located in application properties file. File type can be overriden individually in entities using OverrideFileType annotation.

document-db repository contains more info about this.
Default port for this application is: 8080.

## Running

Run from main class or using bootRun task.
To run this project from commandline using this command:

`./gradlew.bat :bootRun` on windows or `./gradlew :bootRun` on linux.

When application has started the api can be accessed from here:
http://localhost:8080/swagger-ui/index.html#/

Database has 4 movies added including genres on startup.

## Testing

Run test task in IntelliJ Idea or from command line using command:

`./gradlew.bat test -i` on windows or `./gradlew test -i` on linux.
